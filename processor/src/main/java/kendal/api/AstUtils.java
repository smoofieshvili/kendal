package kendal.api;

import java.util.List;

import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.tree.TreeMaker;
import com.sun.tools.javac.util.Name;

import kendal.model.Node;

public interface AstUtils {
    Name nameFromString(String name);

    <T> com.sun.tools.javac.util.List<T> toJCList(List<T> list);

    <T> com.sun.tools.javac.util.List<T> toJCList(T element);

    <T extends JCTree> com.sun.tools.javac.util.List<T> mapNodesToJCListOfObjects(List<Node<T>> listOfNodes);

    /**
     * Will perform deep clone of JCTree.
     * Method is targeted at cloning annotation parameters.
     * @param treeMaker the TreeMaker object obtained from current context
     * @param tree tree to be cloned
     * @return deeply cloned tree object
     */
    <T extends JCTree> T deepClone(TreeMaker treeMaker, T tree);
}
