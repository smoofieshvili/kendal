package kendal.api;

import java.util.Collection;
import java.util.Map;

import javax.lang.model.element.Name;

import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.tree.JCTree.JCAnnotation;
import com.sun.tools.javac.tree.JCTree.JCAssign;
import com.sun.tools.javac.tree.JCTree.JCClassDecl;
import com.sun.tools.javac.tree.JCTree.JCExpressionStatement;
import com.sun.tools.javac.tree.JCTree.JCMethodDecl;
import com.sun.tools.javac.tree.JCTree.JCVariableDecl;
import com.sun.tools.javac.util.Context;

import kendal.api.exceptions.ImproperNodeTypeException;
import kendal.model.Node;

/**
 * Interface for AST modification helper class. Instance of AstHelper is passed to all registered implementations of {@link kendal.api.KendalHandler}.
 * Helper provides wide spectrum of methods that allow for modification, creation or analysis of AST nodes.
 * It also contains more specific helpers which deliver more specialized functionalities.
 */
public interface AstHelper {

    // -  MODIFICATION METHODS ----------------------------------------------------------------------

    /**
     * Adds node to a specified class according to the mode and offset.
     *
     * @param clazz node representing class that is to be modified
     * @param element element to be added to the class
     * @param mode either PREPEND or APPEND, defines whether we add from the beginning or the end
     * @param offset number of lines to be skipped before element is added
     */
    void addElementToClass(Node<JCClassDecl> clazz, Node element, Mode mode, int offset);

    /**
     * Does the same as {@link AstHelper#addElementToClass(Node, Node, Mode, int)} but with APPEND mode and offset = 0.
     */
    default void addElementToClass(Node<JCClassDecl> clazz, Node element) {
        addElementToClass(clazz, element, Mode.APPEND, 0);
    }

    /**
     * Adds node arg representing assignment to a set of arguments of annotation represented by annotationNode.
     */
    void addArgToAnnotation(Node<JCAnnotation> annotationNode, Node<JCAssign> arg);

    /**
     * Adds node representing expression statement to a specified method according to the mode and offset.
     *
     * @param method node representing method that is to be modified
     * @param expressionStatement element to be added to the method
     * @param mode either PREPEND or APPEND, defines whether we add from the beginning or the end
     * @param offset number of lines to be skipped before element is added
     */
    <T extends JCExpressionStatement> void addExpressionStatementToMethod(Node<JCMethodDecl> method,
            Node<T> expressionStatement, Mode mode, int offset);
    /**
     * Does the same as {@link AstHelper#addExpressionStatementToMethod(Node, Node, Mode, int)} but takes into account
     * presence of super() invocation. If present and PREPEND mode is selected, expression statement is added after it.
     */
    <T extends JCExpressionStatement> void addExpressionStatementToConstructor(Node<JCMethodDecl> method,
            Node<T> expressionStatement, Mode mode, int offset) throws ImproperNodeTypeException;

    /**
     * In the given parent node replaces oldNode node with the newNode.
     */
    void replaceNode(Node<? extends JCTree> parent,
                     Node<? extends JCTree> oldNode,
                     Node<? extends JCTree> newNode);



    // -  FINDER METHODS ----------------------------------------------------------------------

    Node<JCVariableDecl> findFieldByNameAndType(Node<JCClassDecl> classDeclNode, Name name);

    Map<Node, Node> getAnnotationSourceMap(Collection<Node> annotationNodes, String sourceQualifiedName);

    Map<String, Object> getAnnotationValues(Node<JCAnnotation> annotationNode);



    // - SPECIFIC HELPERS ----------------------------------------------------------------------

    /**
     * Returns context which allows to obtain objects from javac compiler.
     */
    Context getContext();

    /**
     * Returns Kendal AST Nodes builder which let's you create new AST nodes in easier way.
     */
    AstNodeBuilder getAstNodeBuilder();

    /**
     * Returns Kendal AST validator which contains some basic validations methods.
     */
    AstValidator getAstValidator();

    /**
     * Returns Kendal AST Utils which is a collection of utilities methods. They serve to transform standard objects
     * to javac specific objects.
     */
    AstUtils getAstUtils();

    /**
     * This enum is used for example when adding elements to body of a method or some objects to the declaration of
     * the class. It determines whether addition should happen from the beginning or from the end.
     */
    enum Mode {
        APPEND, PREPEND
    }
}
